﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
	public GameObject target;
	Vector3 pos;

	// Use this for initialization
	void Start () {
		pos = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z - 10); // Save the new position to the camera
		transform.position = new Vector3(pos.x, pos.y, pos.z);			// Slerp, make the transition faster in the middle and slower at the begining and the end
	}
	
	// Update is called once per frame
	void Update () {
		pos = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z - 10); // Save the new position to the camera
		transform.position = Vector3.Slerp(transform.position, pos, Time.deltaTime);			// Slerp, make the transition faster in the middle and slower at the begining and the end
		
	}
}
