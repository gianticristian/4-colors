﻿// PlayerController
using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	public GameController gameController;
	public Transform spawnPoint;
	public string currentColor;
	public float speed;
	public float rotateSpeed;
	public float jumpForce;
	public float JumpPlatformImpulse;
	Animator anim;
	bool canJump;
	public bool canMove;
	int rotationAngle;

	void Awake() {
		anim = GetComponent<Animator>();
		canMove = true;
	}

	// Update is called once per frame
	void Update () {
		if(canMove) {
			// Move Right
			if(Input.GetAxis("Horizontal") > 0) {												// If press Right Key, D, or something like that	
				float translation = speed * Time.deltaTime;													// Store the speed per second
				transform.Translate(translation, 0.0f, 0.0f, relativeTo:Space.World);						// Apply that speed to the translation
			}
			// Move Left
			if(Input.GetAxis("Horizontal") < 0) {												// If press Left Key, A, or something like that
				float translation = speed * Time.deltaTime;
				transform.Translate(-translation, 0.0f, 0.0f, relativeTo:Space.World);						// Use "-" to make it go to the left. Use Space.World so will work fine when you rotate the box
			}
		}
		// Jump
		if(Input.GetKey(KeyCode.Z)) {														// If press 'Z' check and try the jump
			if(canJump) {																				// If the player is in the ground
				GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);	// Add physics force to emulate the jump
				canJump = false;																		// Now the player is not in the ground
			}
		}
		// Rotate
		if(Input.GetKeyDown(KeyCode.X)) {													// If press 'X' rotate once for each time the player press the key
			switch(currentColor) {																		// Look the current color and change it for the next one
				case "Red":
					currentColor = "Green";
					break;
				case "Green":
					currentColor = "Blue";
					break;
				case "Blue":
					currentColor = "Yellow";
					break;
				case "Yellow":
					currentColor = "Red";
					break;
			}
			rotationAngle -= 90;
			Quaternion target = Quaternion.Euler(0, 0, rotationAngle);									// Set the destination
			transform.rotation = Quaternion.Slerp(transform.rotation, target, rotateSpeed);				// Make a smooth rotation
			gameController.GetComponent<GameController>().RotateHUD(currentColor);
		}
	}

	// Check if the player is in the ground and ready to make the big jump!
	void OnCollisionStay2D(Collision2D other) {															// Meantime the collision is active with something
		if(other.gameObject.tag == "Floor") {															// If the other object has the tag "Floor"
			canJump = true;																				// Set the player ready to jump
		}
	}

	public void Die() {																					// Just move to the spawn position
		transform.position = spawnPoint.position;
		anim.SetTrigger("Spawn");
	}


}
