﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour {
	public Transform target;			// This will be the exit of the level
	
	void Update() {
		// http://docs.unity3d.com/ScriptReference/Mathf.Atan2.html
		// Not pretty sure how this works
		float angle = 0;
		Vector3 relative = transform.InverseTransformPoint(target.position);
		angle = Mathf.Atan2(relative.x, relative.y) * Mathf.Rad2Deg;
		transform.Rotate(0,0, -angle);
	}
}
