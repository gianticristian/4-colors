﻿using UnityEngine;
using System.Collections;

public class JumpPlatform : MonoBehaviour {
	public float JumpPlatformImpulse;			// Amount of force impulse that will be apply to the player
	Animator anim;

	void Awake() {
		anim = GetComponent<Animator>();
	}

	void OnCollisionEnter2D(Collision2D other) {
		if(other.gameObject.tag == "Player") {
			anim.SetTrigger("Impulse");			// Call the animation
			other.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector3(0.0f, JumpPlatformImpulse, 0.0f),ForceMode2D.Impulse);	// Add a force impulse to the player
		}
	}
}
