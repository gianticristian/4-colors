﻿using UnityEngine;
using System.Collections;

public class LineConnected : MonoBehaviour {
	public Transform[] points;
	LineRenderer line;
	int pointsTotal;


	// Use this for initialization
	void Start () {
		line = GetComponent<LineRenderer>();				// Get the component to work with it
		pointsTotal = points.GetLength(0);					// Get the total of points(this are all the game objects set it as destinations
		line.SetVertexCount(pointsTotal);					// Set the total of points of the line renderer with the total of destinations points
	
		for(int i = 0; i < pointsTotal; i++) {				// For each point in the array
			line.SetPosition(i, points[i].position);		// Set the position in the same place of the line position
		}


	}
}
