﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {
	public GameObject player;
	public Animator HUDAnim;
	
	void Awake() {
		string firstColor = player.GetComponent<PlayerController>().currentColor;
		switch(firstColor) {
			case "Red":
				HUDAnim.SetTrigger("FirstRed");
				break;
			case "Green":
				HUDAnim.SetTrigger("FirstGreen");
				break;
			case "Blue":
				HUDAnim.SetTrigger("FirstBlue");
				break;
			case "Yellow":
				HUDAnim.SetTrigger("FirstYellow");
				break;
		}
	}

	void OnTriggerExit2D(Collider2D other) {
		if(other.tag == "Player") {
			player.GetComponent<PlayerController>().Die();
		}
	}

	public void RotateHUD(string currentColor) {
		switch(currentColor) {
		case "Red":
			HUDAnim.SetTrigger("RotateRed");
			break;
		case "Green":
			HUDAnim.SetTrigger("RotateGreen");
			break;
		case "Blue":
			HUDAnim.SetTrigger("RotateBlue");
			break;
		case "Yellow":
			HUDAnim.SetTrigger("RotateYellow");
			break;
		}
	}
}
