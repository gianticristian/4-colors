﻿using UnityEngine;
using System.Collections;

public class CheckColor : MonoBehaviour {
	public string color;				// Write the color of this platform
	public Sprite EmptySprite;			// Put the sprite for the platform by default (with no player on it)
	public Sprite FullSprite;			// Put the sprite to change when the player's color is right
	string playerColor;					// Is used to store the curren color of the player and check if is right
	
	void OnCollisionStay2D(Collision2D other) {												
		if(other.gameObject.tag == "Player") {												// Check if the collision is with the player
			playerColor = other.gameObject.GetComponent<PlayerController>().currentColor;	// Store the current color of the player
			if(color != playerColor) {														// Is not the same color?
				other.gameObject.GetComponent<PlayerController>().Die();					// Just die!
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().sprite = FullSprite;				// Is the right color?, ok change the sprite for FullSprite
			}
		}
	}

	void OnCollisionExit2D(Collision2D other) {
		if(other.gameObject.tag == "Player") {												// Is  the player leaving this platform?
			gameObject.GetComponent<SpriteRenderer>().sprite = EmptySprite;					// Set the EmptySprite
		}
	}
}
