﻿using UnityEngine;
using System.Collections;

public class MovePlatform : MonoBehaviour {
	public Transform[] goTo;				// Array with all the destination points
	public int destination;					// The current destination to go

	void Update () {
		transform.position = Vector3.Lerp(transform.position, goTo[destination].position, Time.deltaTime);	// Move the platform to the current destination
	}

	void OnTriggerEnter2D(Collider2D other) {					
		if(other.gameObject.name == goTo[destination].name) {	// If hit the current destination
			if(destination + 1 == goTo.Length) {				// Check if is it the last item of the array
				destination = 0;								// Set it back to 0
			}
			else {
				destination++;									// If is not, select the next destination
			}
		}
	}

	void OnCollisionEnter2D(Collision2D other) {				// This is used to make the player stay in the moving platform
		if(other.gameObject.tag == "Player") {					// If the other object is the player
			other.transform.parent = transform;					// Set this platform as parent of the player, so he will be move with the platform
		}
	}

	void OnCollisionExit2D(Collision2D other) {					// When the player leaves the platform
		if(other.gameObject.tag == "Player") {
			other.transform.parent = null;						// Just say that the player has no parent now
		}
	}


}
